--
--@file
--main.lua
--
--@authors
--John		- Main character Illustration
--Glenn		- Heart and Background Illustration
--Andrew	- Music, Helicopter Illustration
--David		- Coding
--
--@date
--July 13, 2020
--
--
--Will this game be fun? Hopefully!
--




--Provides a function that is used to copy tables.
--
--(??? Lua doesn't have a standard way to copy tables? What?)
--
--(Taken from http://lua-users.org/wiki/CopyTable)
--
--Don't use the 2nd argument, only the 1st!

function deepcopy (orig, copies)

	copies = copies or {}

	local orig_type = type(orig)
	local copy

	if orig_type == 'table' then

		if copies[orig] then

			copy = copies[orig]

		else

			copy = {}
			copies[orig] = copy

			for orig_key, orig_value in next, orig, nil do

				copy [ deepcopy (orig_key, copies)] =

				deepcopy (orig_value, copies)

			end

			setmetatable(copy, deepcopy(getmetatable(orig), copies))

		end

	else --number, string, boolean, etc

		copy = orig

	end

	return copy

end




--Records start time of program

beg_time = love.timer.getTime ();




--What the default size of the window is assumed to be

assumed_win_x = 1920;
assumed_win_y = 1080;




--Ammo bar positions and lengths

ammo_bar_0_x =		40;
ammo_bar_0_y =		950;

ammo_bar_0_x_add =	350;
ammo_bar_0_y_add = 	30;

ammo_bar_0_red =	0.50;
ammo_bar_0_green =	0.70;
ammo_bar_0_blue = 	0.50;
ammo_bar_0_alpha =	1.00;


ammo_bar_1_x =		1880;
ammo_bar_1_y =		950;

ammo_bar_1_x_add =	350;
ammo_bar_1_y_add = 	30;

ammo_bar_1_red =	0.50;
ammo_bar_1_green =	0.70;
ammo_bar_1_blue = 	0.50;
ammo_bar_1_alpha =	1.00;




--Health bar positions and lengths

health_bar_0_x =	40;
health_bar_0_y =	1000

health_bar_0_x_add =	350;
health_bar_0_y_add = 	30;

health_bar_0_red =	0.90;
health_bar_0_green =	0.20;
health_bar_0_blue = 	0.20;
health_bar_0_alpha =	1.00;


health_bar_1_x =	1880;
health_bar_1_y =	1000;

health_bar_1_x_add =	350;
health_bar_1_y_add = 	30;

health_bar_1_red =	0.90;
health_bar_1_green =	0.20;
health_bar_1_blue = 	0.20;
health_bar_1_alpha =	1.00;




--
--Stats of the blue player helicopter
--

p0 = {

	--This is the position (0th derivative of position)

	p_d0_x = assumed_win_x / 4;
	p_d0_y = assumed_win_y / 2;

	p_d0_x_min = 0;
	p_d0_y_min = 0;

	p_d0_x_max = assumed_win_x;
	p_d0_y_max = assumed_win_y;


	--This is the velocity (1st derivative of position)

	p_d1_x = 0;
	p_d1_y = 0;


	--This is the acceleration (2nd derivative of position)

	p_d2_x = 0;
	p_d2_y = 0;


	--This is the jerk (3rd derivative of position)

	p_d3_x = 0;
	p_d3_y = 0;

	p_d3_x_no_press = 0;
	p_d3_y_no_press = 0;

	p_d3_x_keypress = 1100;
	p_d3_y_keypress = 1100;

	p_d3_x_decay_d1_ratio = 14;
	p_d3_y_decay_d1_ratio = 14;

	p_d3_x_decay_d2_ratio = 4;
	p_d3_y_decay_d2_ratio = 4;


	--These are the keyboard controls used to control this player

	up =	"up";
	down =	"down";
	left =	"left";
	right =	"right";
	shoot = "space";


	--This is the amount each p_d*_* value should be multiplied by when a
	--"bounce" off the wall occurs

	p_d1_x_w_bounce = -0.75;
	p_d1_y_w_bounce = -0.75;

	p_d2_x_w_bounce = -0.60;
	p_d2_y_w_bounce = -0.60;

	p_d3_x_w_bounce = -0.30;
	p_d3_y_w_bounce = -0.30;


	--The is the amount each p_d*_* should be multiplied by when a "bounce"
	--off another player occurs

	p_d1_total_p_bounce = 1.05;

	p_d2_total_p_bounce = 1.00;


	--The amount the sprite should be rotated by measured in radians

	rot_rate_x_d1 = 1 / 5000;
	rot_rate_y_d1 = 1 / 5000;

	rot_rate_x_d2 = 0;
	rot_rate_y_d2 = 0;

	rot_rate_x_d3 = 0;
	rot_rate_y_d3 = 0;

	target_rot = 0;

	target_rot_div = 5;

	rot = 0;


	--The file name of the sprite to use for this player

	sprite_file_name = "jogle-game-frames/copter_blue_small.png";


	--Size of the player sprite (this should be determined after loading)

	sprite_len_x = 1;
	sprite_len_y = 1;


	--Amount that the player sprite should be scaled by

	sprite_scale_x = 0.75;
	sprite_scale_y = 0.75;


	--Describes an ellipse centered around (p_d0_x, p_d0_y) which is used
	--for hit detection

	hit_ellipse_a = 115;
	hit_ellipse_b = 40;

	hit_ellipse_offset_x = -20;
	hit_ellipse_offset_y = 10;


	--The amount of ammo and health the copter has

	max_health =	10000;
	cur_health =	10000;

	max_ammo =	60;
	cur_ammo =	25;
	ammo_cost =	1;


	--What position projectiles should be shot from, relative to the
	--coordinate (p_d0_x, p_d0_y)

	project_x = 90;
	project_y = 0;


	--The rotation angle (on top of the helicopter's rotation angle) that
	--projectiles will be shot

	project_rot_mult = 1;

	project_rot_add = 0;


	--Indicates if the copter has been killed

	dead = 0;

};


p0_default = deepcopy (p0);



--
--Stats of the red player helicopter
--

p1 = deepcopy (p0);

p1.p_d0_x = 3 * assumed_win_x / 4;
p1.p_d0_y = assumed_win_y / 2;

p1.p_d3_x_keypress = 1500;
p1.p_d3_y_keypress = 1500;

p1.p_d3_x_decay_d2_ratio = 2;
p1.p_d3_y_decay_d2_ratio = 2;

p1.up = 	"w";
p1.down =	"s";
p1.left =	"a";
p1.right =	"d";
p1.shoot = 	"f";

p1.p_d1_x_w_bounce = -0.90;
p1.p_d1_y_w_bounce = -0.90;

p1.p_d2_x_w_bounce = -0.60;
p1.p_d2_y_w_bounce = -0.60;

p1.p_d3_x_w_bounce = -0.30;
p1.p_d3_y_w_bounce = -0.30;

p1.rot_rate_x_d1 = 1 / 5000;
p1.rot_rate_y_d1 = 1 / 5000;

p1.rot_rate_x_d2 = 1 / 5000;
p1.rot_rate_y_d2 = 1 / 5000;

p1.rot_rate_x_d3 = 0;
p1.rot_rate_y_d3 = 0;

p1.sprite_file_name = "jogle-game-frames/copter_red_small.png";

p1.project_x = 110;
p1.project_y = 0;

p1.project_rot_mult =	1;
p1.project_rot_add =	math.pi;


p1_default = deepcopy (p1);




--FIXME : Make a data structure specifically for the heart's movement

heart = deepcopy (p0);

heart.p_d0_x = assumed_win_x / 2;
heart.p_d0_y = assumed_win_y / 4;

heart.up = 	"9";
heart.down =	"9";
heart.left =	"9";
heart.right =	"9";
heart.shoot = 	"9";

heart.p_d3_x_decay_d1_ratio = 11;
heart.p_d3_y_decay_d1_ratio = 11;

heart.p_d3_x_decay_d2_ratio = 1.5;
heart.p_d3_y_decay_d2_ratio = 1.5;

heart.hit_ellipse_a = 70;
heart.hit_ellipse_b = 110;


heart_default = deepcopy (heart);




--FIXME : Make a data structure specifically for the mountain boy

mboy = deepcopy (p0);

mboy.p_d0_x = assumed_win_x / 2;
mboy.p_d0_y = 3 * assumed_win_y / 4;

mboy.up = 	"9";
mboy.down =	"9";
mboy.left =	"9";
mboy.right =	"9";
mboy.shoot = 	"9";

mboy.p_d3_x_decay_d1_ratio = 11;
mboy.p_d3_y_decay_d1_ratio = 11;

mboy.p_d3_x_decay_d2_ratio = 1.5;
mboy.p_d3_y_decay_d2_ratio = 1.5;

mboy.hit_ellipse_a = 110;
mboy.hit_ellipse_b = 130;


mboy_default = deepcopy (mboy);




--
--Stats of the blue player projectiles
--
--FIXME : It is not very efficient to store all this information per-projectile
--unless it is meant to change per projectile
--

b0 = {

	--Indicates if this projectile is active or not.
	--
	--This is useful for distinguishing between existing and nonexistant
	--projectiles in tables.

	active = 0;


	--Projectile ID (Useful for making projectiles look unique)

	id = 0;


	--The radius of the projectile

	radius = 20;


	--Min / max position of projectiles

	p_d0_x_min = 0;
	p_d0_y_min = 0;

	p_d0_x_max = assumed_win_x;
	p_d0_y_max = assumed_win_y;


	--The position and its derivatives

	p_d0_x = 0;
	p_d0_y = 0;

	p_d1_x = 0;
	p_d1_y = 0;


	--What the position derivatives get multiplied by when they hit a wall

	p_d1_x_w_bounce = -1.00;
	p_d1_y_w_bounce = -1.00;


	--The position derivatives that a projectile will be launched with

	launch_d1 = 105;


	--The amount force the projectiles hit the player copters with

	impact_d3 = 100000;


	--The amount of health the projectiles take out at full speed

	max_damage = 600;


	--Makes the projectile rotate around a center

	rot_radius = 0;

	rot_rate = 4 * math.pi;

	rot = 0;


	--The final position of the projectile after rotation is applied

	final_p_d0_x = 0;
	final_p_d0_y = 0;


	--The time to live for the projectile, and the time that the projectile
	--was launched

	t_max_len = 8;

	t_start = 0;


	--Determines the color of the bullet

	red =		0.00;
	green =		0.00;
	blue =		0.55;
	alpha =		1.00;

}




--
--Stats of the red player projectiles
--

b1 = deepcopy (b0);

b1.launch_d1 =	25;

b1.t_max_len =	16;

b1.rot_radius =	4;
b1.rot_rate =	6 * math.pi;

b1.red = 	1.00;
b1.green =	0.00;
b1.blue =	0.00;
b1.alpha =	1.00;




--
--Blue player projectile list
--

LEN_bl0 = 200;

bl0 = {n = LEN_bl0};

bl0_default = deepcopy (bl0);




--
--Red player projectile list
--

LEN_bl1 = 200;

bl1 = {n = LEN_bl1};

bl1_default = deepcopy (bl1);




--
--Constants that represent precision when calculating ellipse collision
--

EC_RADIUS =		12;
EC_ANG_RADIUS_DIV =	3;




--Indicates whether hit-ellipses should be drawn or not

show_hit_ellipse = false;




--Indicates whether ellipse-collision should be drawn or not

show_ellipse_collide = false;




--This function is used to get the sign of a value

function get_sign (x)

	if (0 > x) then

		return -1;

	end

	if (0 == x) then

		return 0;

	end

	if (0 < x) then

		return 1;

	end

end




--Determines if a point is inside of an ellipse or not

function in_ellipse (e_x, e_y, e_a, e_b, e_ang, x, y)

	local relative_x = x - e_x;
	local relative_y = y - e_y;


	eq =

	(
		((relative_x * math.cos (e_ang) + relative_y * math.sin (e_ang)) ^ 2)

		/

		(e_a ^ 2)
	)

	+

	(
		((relative_x * math.sin (e_ang) - relative_y * math.cos (e_ang)) ^ 2)

		/

		(e_b ^ 2)
	)

	return (eq <= 1);

end




--Checks if 2 player copters collide with one another

function ellipse_collide (

	e0_x,
	e0_y,
	e0_a,
	e0_b,
	e0_ang,

	e1_x,
	e1_y,
	e1_a,
	e1_b,
	e1_ang,

	rad_step,
	ang_rad_step_div
)


	--Check a circle surrounding each player copter, and see if points
	--sampled from it are in both player copterstruehit ellipses.
	--
	--Only points from both ellipses are really needed, iterating through a
	--circular radius is easier.

	--Determine the radius to use for the iteration circle

	local e0_r = math.max (e0_a, e0_b);

	local e1_r = math.max (e1_a, e1_b);


	--print ("01_r = ", e0_r);

	--print ("e1_r = ", e1_r);


	--Iterate through a full circle of angles and a radiuses using
	--(ang_step, rad_step).
	--
	--Note, the "+ (c0_r % rad_step))" ensures that the radius upper-limit
	--is divisble by rad_step

	local ang = 0;

	local rad = 0;

	local result = {is_hit = 0, x = 0, y = 0};


	for rad = 0, (e0_r + (e0_r % rad_step)), rad_step do

		for ang = 0, (2 * math.pi), ((2 * math.pi) / (rad / ang_rad_step_div)) do


			--Fetch the (x, y) coordinate associated with this
			--radius and angle

			local x = e0_x + (rad * math.cos (ang));

			local y = e0_y + (rad * math.sin (ang));


			--Draw this (x, y) point if requested

			if (show_ellipse_collide) then

				--print ("rad = ", rad);
				--print ("ang = ", ang);


				love.graphics.setPointSize (2.0);

				point = {

					{
						x,
						y,
						1.0,
						1.0,
						1.0,
						1.0
					}
				};

				love.graphics.points (point);

			end


			--Check if the point is in either ellipse

			in_e0 =

			in_ellipse (

				e0_x,
				e0_y,
				e0_a,
				e0_b,
				e0_ang,
				x,
				y
			);


			in_e1 =

			in_ellipse (

				e1_x,
				e1_y,
				e1_a,
				e1_b,
				e1_ang,
				x,
				y
			);


			if (in_e0 and in_e1) then

				--Draw the collision if requested

				if (show_ellipse_collide) then

					love.graphics.setPointSize (12.0);

					point = {

						{
							x,
							y,
							0.75,
							0.75,
							0.0,
							1.0
						}
					};

					love.graphics.points (point);

				end


				result.is_hit =		1;
				result.x =		x;
				result.y =		y;


				return result;

			end

		end

	end


	--Indicate that no point tested in either ellipse hit one another,
	--and provide an arbitrary (x, y) coordinate of (0, 0)

	result.is_hit =		0;
	result.x =		0;
	result.y =		0;

	return result;

end




--Determines if a point is inside of a player copter's hit-ellipse or not

function in_p_hit_ellipse (p, x, y)


	return in_ellipse (

		p.p_d0_x + p.hit_ellipse_offset_x,
		p.p_d0_y + p.hit_ellipse_offset_y,
		p.hit_ellipse_a * p.sprite_scale_x,
		p.hit_ellipse_b * p.sprite_scale_y,
		p.rot,
		x,
		y
	);

end




--Checks if the position of 2 players is colliding via their hit-ellipses, and
--updates their position derivatives (but not the position itself)

function collide_players (p0, p1)


	--Check if the 2 players are colliding

	collide = ellipse_collide (

		p0.p_d0_x + p0.hit_ellipse_offset_x,
		p0.p_d0_y + p0.hit_ellipse_offset_y,
		p0.hit_ellipse_a * p0.sprite_scale_x,
		p0.hit_ellipse_b * p0.sprite_scale_y,
		p0.rot,

		p1.p_d0_x + p1.hit_ellipse_offset_x,
		p1.p_d0_y + p1.hit_ellipse_offset_y,
		p1.hit_ellipse_a * p1.sprite_scale_x,
		p1.hit_ellipse_b * p1.sprite_scale_y,
		p1.rot,

		EC_RADIUS,
		EC_ANG_RADIUS_DIV
	);


	print ("collide.is_hit = ", collide.is_hit);
	print ("collide.x = ", collide.x);
	print ("collide.y = ", collide.y);


	if (1 == collide.is_hit) then


		--Determine the magnitude of each position derivative vector's
		--hypotenuse, and the angle from the collision and the x-axis

		p0_hyp_d1 = math.sqrt ((p0.p_d1_x ^ 2) + (p0.p_d1_y ^ 2));
		p0_hyp_d2 = math.sqrt ((p0.p_d2_x ^ 2) + (p0.p_d2_y ^ 2));

		p1_hyp_d1 = math.sqrt ((p1.p_d1_x ^ 2) + (p1.p_d1_y ^ 2));
		p1_hyp_d2 = math.sqrt ((p1.p_d2_x ^ 2) + (p1.p_d2_y ^ 2));


		ang = math.atan ((p0.p_d0_y - p1.p_d0_y) / (p1.p_d0_x - p0.p_d0_x));


		--Average out the magnitude the hypotenuses so that both
		--players bounce even if only 1 is moving

		hyp_d1_avg = (p0_hyp_d1 + p1_hyp_d1) / 2;

		hyp_d2_avg = (p0_hyp_d2 + p1_hyp_d2) / 2;


		--Update the player's position derivatives

		if (p0.p_d0_x > p1.p_d0_x) then

			p0.p_d1_x =

			p0.p_d1_total_p_bounce * hyp_d1_avg * math.cos (ang);

			p0.p_d1_y =

			p0.p_d1_total_p_bounce * hyp_d1_avg * math.sin (ang);

			p0.p_d2_x =

			p0.p_d2_total_p_bounce * hyp_d2_avg * math.cos (ang);

			p0.p_d2_y =

			p0.p_d2_total_p_bounce * hyp_d2_avg * math.sin (ang);


			p1.p_d1_x =

			p1.p_d1_total_p_bounce * hyp_d1_avg * math.cos (ang + math.pi);

			p1.p_d1_y =

			p1.p_d1_total_p_bounce * hyp_d1_avg * math.sin (ang + math.pi);

			p1.p_d2_x =

			p1.p_d2_total_p_bounce * hyp_d2_avg * math.cos (ang + math.pi);

			p1.p_d2_y =

			p1.p_d2_total_p_bounce * hyp_d2_avg * math.sin (ang + math.pi);

		else

			p0.p_d1_x =

			p0.p_d1_total_p_bounce * hyp_d1_avg * math.cos (ang + math.pi);

			p0.p_d1_y =

			p0.p_d1_total_p_bounce * hyp_d1_avg * math.sin (ang + math.pi);

			p0.p_d2_x =

			p0.p_d2_total_p_bounce * hyp_d2_avg * math.cos (ang + math.pi);

			p0.p_d2_y =

			p0.p_d2_total_p_bounce * hyp_d2_avg * math.sin (ang + math.pi);


			p1.p_d1_x =

			p1.p_d1_total_p_bounce * hyp_d1_avg * math.cos (ang);

			p1.p_d1_y =

			p1.p_d1_total_p_bounce * hyp_d1_avg * math.sin (ang);

			p1.p_d2_x =

			p1.p_d2_total_p_bounce * hyp_d2_avg * math.cos (ang);

			p1.p_d2_y =

			p1.p_d2_total_p_bounce * hyp_d2_avg * math.sin (ang);

		end

	end


	return collide;

end




--This function is used to update the player helicopter position

function update_player (p, fra_time)


	--This reacts to keypresses

	--Move the player helicopter in response to keypresses by updating its
	--"jerk"


	--Give the "jerk" a default value

	p.p_d3_x = p.p_d3_x_no_press;
	p.p_d3_y = p.p_d3_y_no_press;


	--Apply decay to the helicopter's movement

	p.p_d3_x =

	p.p_d3_x

	+

	(-1 * p.p_d1_x * p.p_d3_x_decay_d1_ratio)

	+

	(-1 * p.p_d2_x * p.p_d3_x_decay_d2_ratio);


	p.p_d3_y =

	p.p_d3_y

	+

	(-1 * p.p_d1_y * p.p_d3_y_decay_d1_ratio)

	+

	(-1 * p.p_d2_y * p.p_d3_y_decay_d2_ratio);


	--Apply the player's inputs to the player's "jerk"

	if (love.keyboard.isDown (p.up)) then

		p.p_d3_y =

		math.min (

			(p.p_d3_y - p.p_d3_y_keypress),
			(-1 * p.p_d3_y_keypress)
		);

	elseif (love.keyboard.isDown (p.down)) then

		p.p_d3_y =

		math.max (

			(p.p_d3_y + p.p_d3_y_keypress),
			(p.p_d3_y_keypress)
		);

	end


	if (love.keyboard.isDown (p.left)) then

		p.p_d3_x =

		math.min (

			(p.p_d3_x - p.p_d3_x_keypress),
			(-1 * p.p_d3_x_keypress)
		);

	elseif (love.keyboard.isDown (p.right)) then

		p.p_d3_x =

		math.max (

			(p.p_d3_x + p.p_d3_x_keypress),
			(p.p_d3_x_keypress)
		);
	end


	--Move the helicopter by updating all the "derivatives of position"

	p.p_d2_x = p.p_d2_x + (p.p_d3_x * fra_time);
	p.p_d2_y = p.p_d2_y + (p.p_d3_y * fra_time);

	p.p_d1_x = p.p_d1_x + (p.p_d2_x * fra_time);
	p.p_d1_y = p.p_d1_y + (p.p_d2_y * fra_time);

	p.p_d0_x = p.p_d0_x + (p.p_d1_x * fra_time);
	p.p_d0_y = p.p_d0_y + (p.p_d1_y * fra_time);


	--Determine the amount the player copter should be rotated by

	p.target_rot =

	(p.p_d1_x * p.rot_rate_x_d1 * math.pi)

	+

	(p.p_d2_x * p.rot_rate_x_d2 * math.pi)

	+

	(p.p_d3_x * p.rot_rate_x_d3 * math.pi)

	+

	(p.p_d1_y * p.rot_rate_y_d1 * math.pi)

	+

	(p.p_d2_y * p.rot_rate_y_d2 * math.pi)

	+

	(p.p_d3_y * p.rot_rate_y_d3 * math.pi);


	p.rot = p.rot + ((p.target_rot - p.rot) / p.target_rot_div);


	--Apply the maximum position to the player helicopter

	local hit_x_min =

	in_p_hit_ellipse (

		p,
		p.p_d0_x_min,
		p.p_d0_y
	);


	local hit_x_max =

	in_p_hit_ellipse (

		p,
		p.p_d0_x_max,
		p.p_d0_y
	);


	local hit_y_min =

	in_p_hit_ellipse (

		p,
		p.p_d0_x,
		p.p_d0_y_min
	);


	local hit_y_max =

	in_p_hit_ellipse (

		p,
		p.p_d0_x,
		p.p_d0_y_max
	);


	--FIXME : Does this section need to take into account the
	--p.hit_ellipse_offset_* values when comparing against #p.p_d0_*_min
	--and #p.p_d0_*_max?

	if (hit_x_min or (p.p_d0_x < p.p_d0_x_min)) then

		p.p_d0_x = p.p_d0_x_min;


		--FIXME : Find a more efficient way of keeping players in
		--bounds while respecting hit-ellipse

		while (in_p_hit_ellipse (p, p.p_d0_x_min, p.p_d0_y)) do

			p.p_d0_x =

			p.p_d0_x + (0.025 * p.sprite_len_x * p.sprite_scale_x);

		end

		p.p_d1_x = math.abs (p.p_d1_x_w_bounce * p.p_d1_x);


		p.p_d2_x = math.abs (p.p_d2_x_w_bounce * p.p_d2_x);


		p.p_d3_x = math.abs (p.p_d3_x_w_bounce * p.p_d3_x);

	end


	if (hit_x_max or (p.p_d0_x > p.p_d0_x_max)) then

		p.p_d0_x = p.p_d0_x_max;


		--FIXME : Find a more efficient way of keeping players in
		--bounds while respecting hit-ellipse

		while (in_p_hit_ellipse (p, p.p_d0_x_max, p.p_d0_y)) do

			p.p_d0_x =

			p.p_d0_x - (0.025 * p.sprite_len_x * p.sprite_scale_x);

		end

		p.p_d1_x = -1 * math.abs (p.p_d1_x_w_bounce * p.p_d1_x);


		p.p_d2_x = -1 * math.abs (p.p_d2_x_w_bounce * p.p_d2_x);


		p.p_d3_x = -1 * math.abs (p.p_d3_x_w_bounce * p.p_d3_x);

	end

	if (hit_y_min or (p.p_d0_y < p.p_d0_y_min)) then

		p.p_d0_y = p.p_d0_y_min;


		--FIXME : Find a more efficient way of keeping players in
		--bounds while respecting hit-ellipse

		while (in_p_hit_ellipse (p, p.p_d0_x, p.p_d0_y_min)) do

			p.p_d0_y =

			p.p_d0_y + (0.025 * p.sprite_len_y * p.sprite_scale_y);

		end

		p.p_d1_y = math.abs (p.p_d1_y_w_bounce * p.p_d1_y);


		p.p_d2_y = math.abs (p.p_d2_y_w_bounce * p.p_d2_y);


		p.p_d3_y = math.abs (p.p_d3_y_w_bounce * p.p_d3_y);

	end


	if (hit_y_max or (p.p_d0_y > p.p_d0_y_max)) then

		p.p_d0_y = p.p_d0_y_max;


		--FIXME : Find a more efficient way of keeping players in
		--bounds while respecting hit-ellipse

		while (in_p_hit_ellipse (p, p.p_d0_x, p.p_d0_y_max)) do

			p.p_d0_y =

			p.p_d0_y - (0.025 * p.sprite_len_y * p.sprite_scale_y);

		end

		p.p_d1_y = -1 * math.abs (p.p_d1_y_w_bounce * p.p_d1_y);


		p.p_d2_y = -1 * math.abs (p.p_d2_y_w_bounce * p.p_d2_y);


		p.p_d3_y = -1 * math.abs (p.p_d3_y_w_bounce * p.p_d3_y);

	end


	--print ("p.p_d0_x_max = ", p.p_d0_x_max);
	--print ("p.p_d0_y_max = ", p.p_d0_y_max);


	--print ("p.p_d0_x = ", p.p_d0_x);
	--print ("p.p_d0_y = ", p.p_d0_y);

	--print ("p.p_d1_x = ", p.p_d1_x);
	--print ("p.p_d1_y = ", p.p_d1_y);

	--print ("p.p_d2_x = ", p.p_d2_x);
	--print ("p.p_d2_y = ", p.p_d2_y);

	--print ("p.p_d3_x = ", p.p_d3_x);
	--print ("p.p_d3_y = ", p.p_d3_y);


	return p;

end




--Adds a projectile to the projectile list

function add_projectile (bl, b, p, cur_time)


	--If the player is dead, do not shoot

	if (1 == p.dead) then

		return;

	end


	--If the player does not have enough ammo, do not perform a shot

	if (0 > p.cur_ammo) then

		return;

	end


	--Search for the first inactive value in bl, which represents an available space
	--to place a bullet.
	--
	--If there is none, just don't add a projectile.

	local ind = 0;

	local b_sel = -1;


	for ind = 1, #bl, (ind + 1) do

		if (0 == bl [ind].active) then

			b_sel = ind;

		end

	end


	if (-1 == b_sel) then

		return;

	end


	--By this point, a shot is being taken, so decrement the ammo count

	p.cur_ammo = p.cur_ammo - p.ammo_cost;


	--Add a new projectile entry in the table

	bl [b_sel] = deepcopy (b);

	b = bl [b_sel];

	b.active = 1;


	--Find the angle that the projectile should be pointed

	project_ang =

	((p.project_rot_mult * p.rot) + p.project_rot_add)


	--Set the initial position of the projectile

	b.p_d0_x =

	p.p_d0_x + (p.project_x * math.cos (project_ang)) - (p.project_y * math.sin (project_ang));


	b.p_d0_y =

	p.p_d0_y + (p.project_y * math.cos (project_ang)) + (p.project_x * math.sin (project_ang));


	--Set the initial movement of the projectile

	b.p_d1_x = b.launch_d1 * math.cos (project_ang);


	b.p_d1_y = b.launch_d1 * math.sin (project_ang);


	--print ("b.p_d0_x = ", bl [b_sel].p_d0_x);
	--print ("b.p_d0_y = ", bl [b_sel].p_d0_y);


	--Set the start time of the projectile

	b.t_start = cur_time;


	--Copy modification (if any) back into #bl

	bl [b_sel] = b;

end




--Process the movement of projectiles

function move_projectiles (bl, pl, cur_time, fra_time)


	--Iterate through all the projectiles to remove ones that are too old

	local b_sel = 0;

	for b_sel = 1, #bl, (b_sel + 1) do

		--Select the projectile

		b = bl [b_sel];


		if (0 == b.active) then

			--Do nothing, this element is removed

		else

			--Check if this element is too old

			--print ("b_sel = ", b_sel);
			--print ("#bl =   ", #bl);
			--print ("b.p_d0_x = ", bl [b_sel].p_d0_x);
			--print ("b.p_d0_y = ", bl [b_sel].p_d0_y);


			--Determine the amount of time the projectile has been active

			t_diff = cur_time - b.t_start;


			--If the projectile is out of date, then deactivate it

			if (t_diff >= b.t_max_len) then

				b.active = 0;

			end


			--Copy modification (if any) back into #bl

			bl [b_sel] = b;


		end

	end


	--Iterate through all the projectiles to update their movement

	for b_sel = 1, #bl, (b_sel + 1) do

		--Select the projectile

		b = bl [b_sel];


		if (0 == b.active) then

			--Do nothing because projectile is inactive, let loop
			--iterate

		else

			--Determine the amount of time the projectile has been
			--active

			t_diff = cur_time - b.t_start;


			--Update position of projectile

			b.p_d0_x =

			b.p_d0_x + (b.p_d1_x * (b.t_max_len - t_diff)) * fra_time;


			b.p_d0_y =

			b.p_d0_y + (b.p_d1_y * (b.t_max_len - t_diff)) * fra_time;


			--Apply the rotation to the position

			b.rot =

			b.rot_rate * t_diff;


			b.final_p_d0_x =

			b.p_d0_x + b.rot_radius * math.cos (b.rot);


			b.final_p_d0_y =

			b.p_d0_y + b.rot_radius * math.sin (b.rot);


			--Keep projectiles in bounds
			--
			--Note, b.final_p_d0_* is the position after rotation,
			--while b.p_d0_* is the center of rotation. Because the
			--final position is what hits the wall, we have to
			--propagate the effects to the center of rotation to
			--bounce it correctly.
			--
			--FIXME : (Is that done correctly here?)

			if ((b.final_p_d0_x - b.radius) < b.p_d0_x_min) then

				b.final_p_d0_x = b.p_d0_x_min + b.radius;

				b.rot_rate = -1 * b.rot_rate;


				b.p_d1_x = math.abs (b.p_d1_x_w_bounce * b.p_d1_x);


				b.p_d0_x = b.final_p_d0_x + (b.rot_radius * math.cos (b.rot));

				b.p_d0_y = b.final_p_d0_y + (b.rot_radius * math.sin (b.rot));

			end


			if ((b.final_p_d0_x + b.radius) > b.p_d0_x_max) then

				b.final_p_d0_x = b.p_d0_x_max - b.radius;

				b.rot_rate = -1 * b.rot_rate;


				b.p_d1_x = -1.0 * math.abs (b.p_d1_x_w_bounce * b.p_d1_x);


				b.p_d0_x = b.final_p_d0_x - (b.rot_radius * math.cos (b.rot));

				b.p_d0_y = b.final_p_d0_y + (b.rot_radius * math.sin (b.rot));

			end

			if ((b.final_p_d0_y - b.radius) < b.p_d0_y_min) then

				b.final_p_d0_y = b.p_d0_y_min + b.radius;

				b.rot_rate = -1 * b.rot_rate;


				b.p_d1_y = math.abs (b.p_d1_y_w_bounce * b.p_d1_y);


				b.p_d0_y = b.final_p_d0_y + (b.rot_radius * math.cos (b.rot));

				b.p_d0_y = b.final_p_d0_y + (b.rot_radius * math.sin (b.rot));

			end


			if ((b.final_p_d0_y + b.radius) > b.p_d0_y_max) then

				b.final_p_d0_y = b.p_d0_y_max - b.radius;

				b.rot_rate = -1 * b.rot_rate;


				b.p_d1_y = -1.0 * math.abs (b.p_d1_y_w_bounce * b.p_d1_y);


				b.p_d0_y = b.final_p_d0_y - (b.rot_radius * math.cos (b.rot));

				b.p_d0_y = b.final_p_d0_y + (b.rot_radius * math.sin (b.rot));

			end


			--Copy modification (if any) back into #bl

			bl [b_sel] = b;

		end

	end

end




--Draws the projectiles in a projectile list

function draw_projectiles (bl, cur_time)

	--Iterate through all the projectiles to remove ones that

	local b_sel = 0;

	for b_sel = 1, #bl, (b_sel + 1) do

		--Select the projectile

		b = bl [b_sel];


		--Check if the projectile is active

		if (0 == b.active) then

			--Do nothing, projectile is inactive

		else

			--Determine the amount of time the projectile has been
			--active

			t_diff = cur_time - b.t_start;


			local fullness =

			(1.5 * (1 - (t_diff / b.t_max_len)));


			fullness = math.min (fullness, 1.0);


			love.graphics.setPointSize (10.0);

			point = {

				{
					b.final_p_d0_x,
					b.final_p_d0_y,
					b.red,
					b.green,
					b.blue,
					fullness * b.alpha
				}
			};

			love.graphics.points (point);

		end

	end

end




--Processes projectile hits

function hit_projectiles (bl, p, cur_time)


	--Iterate through all the projectiles to remove ones that are too old

	local b_sel = 0;

	for b_sel = 1, #bl, (b_sel + 1) do

		--Select the projectile

		b = bl [b_sel];


		--Check if the projectile is active

		if (0 == b.active) then

			--Do nothing, projectile is inactive

		else


			local t_diff = cur_time - b.t_start;


			--Determine if the bullet has hit the player

			local hit = in_ellipse (

				p.p_d0_x + p.hit_ellipse_offset_x,
				p.p_d0_y + p.hit_ellipse_offset_y,
				p.hit_ellipse_a * p.sprite_scale_x,
				p.hit_ellipse_b * p.sprite_scale_y,
				p.rot,
				b.p_d0_x,
				b.p_d0_y
			);


			if (true == hit) then

				b.active = 0;


				p.cur_health =

				p.cur_health - (b.max_damage * ((b.t_max_len - t_diff) / b.t_max_len));


				if (0 >= p.cur_health) then

					p.sprite_scale_x = 0;
					p.sprite_scale_y = 0;

					p.dead = 1;

				end


				p.cur_health =

				math.max (p.cur_health, 0);

			end


			--Copy modification (if any) back into #bl

			bl [b_sel] = b;

		end

	end

end






--Draws a hitbox in a very arbitrary fashion around a player

function draw_hit_ellipse (p)

	love.graphics.setPointSize (5.0);

	for y = 0, 2.5 * p.sprite_len_y, (2.5 * p.sprite_len_y / 20) do

		for x = 0, 1.5 * p.sprite_len_x, (1.5 * p.sprite_len_x / 20) do


			screen_x = x + p.p_d0_x - (1.5 * p.sprite_len_x / 2);

			screen_y = y + p.p_d0_y - (2.5 * p.sprite_len_y / 2);


			is_hit =

			in_p_hit_ellipse (

				p,
				screen_x,
				screen_y
			);


			if (is_hit) then

				point = {

					{
						screen_x,
						screen_y,
						0.0,
						1.0,
						0.0,
						1.0
					}
				};

			else

				point = {

					{
						screen_x,
						screen_y,
						1.0,
						0.0,
						1.0,
						1.0
					}
				};

			end


			love.graphics.points (point);

		end

	end

end




function love.keyreleased (key, scancode)

	--Detect when a user requests hit-ellipses viewing

	if (key == "h") then

		show_hit_ellipse = not show_hit_ellipse;

	end


	--Detect when a user requests ellipse-collision viewing

	if (key == "j") then

		show_ellipse_collide = (not show_ellipse_collide);

	end


	--Detect when projectiles are fired

	cur_time = love.timer.getTime ();

	if (key == p0.shoot) then

		add_projectile (bl0, b0, p0, cur_time);

	end

	if (key == p1.shoot) then

		add_projectile (bl1, b1, p1, cur_time);

	end


	--Detect when a reset has been requested

	if (key == "p") then

		p0 =	deepcopy (p0_default);
		p1 =	deepcopy (p1_default);
		heart =	deepcopy (heart_default);
		mboy = 	deepcopy (mboy_default);


		love.load ();

		bgm : stop ();

	end
end




function love.load ()


	--Initialize the projectile tables

	local b_sel = 0;

	for b_sel = 1, LEN_bl0, (b_sel + 1) do

		bl0 [b_sel] = deepcopy (b0);

		bl0 [b_sel].active = 0;

	end

	for b_sel = 1, LEN_bl1, (b_sel + 1) do

		bl1 [b_sel] = deepcopy (b1);

		bl1 [b_sel].active = 0;

	end


	--Load some sprites

	blue_copter =

	love.graphics.newImage (p0.sprite_file_name);


	red_copter =

	love.graphics.newImage (p1.sprite_file_name);


	heart_pic =

	love.graphics.newImage ("jogle-game-frames/heart_small.png");


	main_guy =

	love.graphics.newImage ("jogle-game-frames/main_guy_small.png");


	background =

	love.graphics.newImage ("jogle-game-frames/background.png");


	--Determine the size of some of the loaded sprites

	p0.sprite_len_x =	blue_copter : getPixelWidth ();
	p0.sprite_len_y =	blue_copter : getPixelHeight ();

	p1.sprite_len_x =	red_copter : getPixelWidth ();
	p1.sprite_len_y =	red_copter : getPixelHeight ();

	heart.sprite_len_x =	heart_pic : getPixelWidth ();
	heart.sprite_len_y =	heart_pic : getPixelHeight ();

	mboy.sprite_len_x =	main_guy : getPixelWidth ();
	mboy.sprite_len_y =	main_guy : getPixelHeight ();


	--Load some music

	bgm =

	love.audio.newSource (

		"jogle-game-sounds/jogles_heli_attack_song.wav",
		"stream"
	);


	bgm:setLooping (true);
	bgm:play ();

end




function love.draw ()


	--Keep track of the current time

	cur_time = love.timer.getTime ();

	del_time = cur_time - beg_time;

	fra_time = love.timer.getDelta ();


	--Pretend that the screen is size (assumed_win_x, assumed_win_y).
	--
	--Then, determine the actual window size, and then scale the screen contents to it

	scale_win_x = love.graphics.getWidth () / assumed_win_x;

	scale_win_y = love.graphics.getHeight () / assumed_win_y;


	love.graphics.scale (scale_win_x, scale_win_y);


	--Check if the players collide at their current position

	collide_players (p0, p1);

	p0_get_health = collide_players (p0, heart);
	p0_get_ammo =	collide_players (p0, mboy);

	p1_get_health = collide_players (p1, heart);
	p1_get_ammo =	collide_players (p1, mboy);

	collide_players (mboy, heart);


	--FIXME : Place this in a better place
	--
	--Add health and ammo to players when they collide with the heart and
	--mountain boy

	if (1 == p0_get_health.is_hit) then

		p0.cur_health = math.min (p0.cur_health + 700, p0.max_health);

	end

	if (1 == p0_get_ammo.is_hit) then

		p0.cur_ammo = math.min (p0.cur_ammo + 20, p0.max_ammo);

	end

	if (1 == p1_get_health.is_hit) then

		p1.cur_health = math.min (p1.cur_health + 700, p1.max_health);

	end

	if (1 == p1_get_ammo.is_hit) then

		p1.cur_ammo = math.min (p1.cur_ammo + 20, p1.max_ammo);

	end


	--Update the player positions

	p0 =	update_player (p0, fra_time);
	p1 =	update_player (p1, fra_time);
	heart =	update_player (heart, fra_time);
	mboy =	update_player (mboy, fra_time);


	--Update the projectile positions

	move_projectiles (bl0, {p0, p1}, cur_time, fra_time);
	move_projectiles (bl1, {p0, p1}, cur_time, fra_time);


	--Process projectile hits

	--hit_projectiles (bl0, p0, cur_time);
	hit_projectiles (bl1, p0, cur_time);

	hit_projectiles (bl0, p1, cur_time);
	--hit_projectiles (bl1, p1, cur_time);




	--Draw the background on the screen.

	love.graphics.draw (
		background,
		0,
		0,
		0,
		(assumed_win_x / background : getPixelWidth ()),
		(assumed_win_y / background : getPixelHeight ()),
		0,
		0
	);


	--Draw characters to the screen.
	--
	--All of the characters are assumed to be 300x300

	char_x = 300;
	char_y = 300;


	love.graphics.draw (
		blue_copter,
		p0.p_d0_x,
		p0.p_d0_y,
		p0.rot,
		p0.sprite_scale_x,
		p0.sprtie_scale_y,
		(p0.sprite_len_x / 2),
		(p0.sprite_len_y / 2)
	);

	love.graphics.draw (
		red_copter,
		p1.p_d0_x,
		p1.p_d0_y,
		p1.rot,
		p1.sprite_scale_x,
		p1.sprite_scale_y,
		(p1.sprite_len_x / 2),
		(p1.sprite_len_y / 2)
	);

	love.graphics.draw (
		heart_pic,
		heart.p_d0_x,
		heart.p_d0_y,
		heart.rot,
		heart.sprite_scale_x,
		heart.sprite_scale_y,
		(heart.sprite_len_x / 2),
		(heart.sprite_len_y / 2)
	);

	love.graphics.draw (
		main_guy,
		mboy.p_d0_x,
		mboy.p_d0_y,
		mboy.rot,
		mboy.sprite_scale_x,
		mboy.sprite_scale_y,
		(mboy.sprite_len_x / 2),
		(mboy.sprite_len_y / 2)
	);


	--Draw the ammo bars

	love.graphics.setColor (

		ammo_bar_0_red,
		ammo_bar_0_green,
		ammo_bar_0_blue,
		ammo_bar_0_alpha
	);


	love.graphics.rectangle (

		"fill",
		ammo_bar_0_x,
		ammo_bar_0_y,
		ammo_bar_0_x_add * (p0.cur_ammo / p0.max_ammo),
		ammo_bar_0_y_add
	);


	love.graphics.setColor (

		ammo_bar_1_red,
		ammo_bar_1_green,
		ammo_bar_1_blue,
		ammo_bar_1_alpha
	);


	love.graphics.rectangle (

		"fill",
		ammo_bar_1_x - (ammo_bar_1_x_add * (p1.cur_ammo / p1.max_ammo)),
		ammo_bar_1_y,
		ammo_bar_1_x_add * (p1.cur_ammo / p1.max_ammo),
		ammo_bar_1_y_add
	);



	--Draw the health bars

	love.graphics.setColor (

		health_bar_0_red,
		health_bar_0_green,
		health_bar_0_blue,
		health_bar_0_alpha
	);


	love.graphics.rectangle (

		"fill",
		health_bar_0_x,
		health_bar_0_y,
		health_bar_0_x_add * (p0.cur_health / p0.max_health),
		health_bar_0_y_add
	);


	love.graphics.setColor (

		health_bar_1_red,
		health_bar_1_green,
		health_bar_1_blue,
		health_bar_1_alpha
	);


	love.graphics.rectangle (

		"fill",
		health_bar_1_x - (health_bar_1_x_add * (p1.cur_health / p1.max_health)),
		health_bar_1_y,
		health_bar_1_x_add * (p1.cur_health / p1.max_health),
		health_bar_1_y_add
	)



	--Reset the color multiplier

	love.graphics.setColor (

		1.0,
		1.0,
		1.0,
		1.0
	);


	--Draw all the projectiles

	draw_projectiles (bl0, cur_time);
	draw_projectiles (bl1, cur_time);


	--Draw hitboxes when the user requested it

	if (show_hit_ellipse) then

		draw_hit_ellipse (p0);
		draw_hit_ellipse (p1);
		draw_hit_ellipse (heart);
		draw_hit_ellipse (mboy);

	end


	--Determine if the coopters collide with each other (debug)

	ellipse_collide (

		p0.p_d0_x + p0.hit_ellipse_offset_x,
		p0.p_d0_y + p0.hit_ellipse_offset_y,
		p0.hit_ellipse_a * p0.sprite_scale_x,
		p0.hit_ellipse_b * p0.sprite_scale_y,
		p0.rot,

		p1.p_d0_x + p1.hit_ellipse_offset_x,
		p1.p_d0_y + p1.hit_ellipse_offset_y,
		p1.hit_ellipse_a * p1.sprite_scale_x,
		p1.hit_ellipse_b * p1.sprite_scale_y,
		p1.rot,

		EC_RADIUS,
		EC_ANG_RADIUS_DIV
	);


end

